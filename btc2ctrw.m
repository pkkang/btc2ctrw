clear all; clc; 
%%
load('BTCs_RTDs.mat')

xLoc = [5:5:80 80.3];
km = [0 0 10.^[-16:-10] [2 5 8 10]*1e-10];

PVI = 137.96; % [sec]

%% == first(x=10) & last(x=80) jump step
ixLoc1 = [2];
xLoc1 = xLoc(ixLoc1);

ixLocEnd = [16];
xLocEnd = xLoc(ixLocEnd);


for iCase =  [1 2 9 13] 
%% ====== binning settings
centers = RTDs{iCase}(:,1)/PVI; % cell centers
centersDiff = diff(centers);
edges = nan(length(centers)+1,1);

edges(1) = centers(1)-centersDiff(1)/2; %left edge for cell i=1
edges(2) = centers(1)+centersDiff(1)/2; %right edge for cell i=1
unevenSplit = false;
for i = 2:length(centers)-1
    if abs(centersDiff(i)-centersDiff(i-1))>0.01 
       if unevenSplit 
           binSpanTmp = centersDiff(i-1)-centersDiff(i-2)/2;
       else
           binSpanTmp = centersDiff(i-1)/2;
       end
       edges(i+1) = edges(i)+binSpanTmp; %right edge for cell i
       unevenSplit = true;
    else
       edges(i+1) = centers(i)+centersDiff(i-1)/2; %right edge for cell i
       unevenSplit = false;
    end
end
edges(end) = centers(end)+centersDiff(end)/2; % the most right edge
binSpans = diff(edges);

logTransitTime  = log10(centers);
logTransitTimeEdges = log10(edges);

%% ====== pdf & cdf of transition times
pdf1 = real(RTDs{iCase}(:,ixLoc1+1));
pdf1 = pdf1/sum(pdf1.*binSpans);

cdf1 = cumsum(binSpans.*pdf1);
[~,ind] = unique(cdf1);

%% ====== CTRW simulation
rng('default')
nPtcl = 10e+5;

nJump = round(xLocEnd/xLoc1);
passageTime = zeros(nPtcl,1);
for iJump = 1:nJump
    ur = rand(nPtcl,1);
    transitTime = 10.^interp1(cdf1(ind),logTransitTime(ind),ur,[],'extrap');
    passageTime = passageTime + transitTime;
end

counts = histcounts(log10(passageTime),logTransitTimeEdges);
countsNorm = counts'/sum(counts);

RTD_CTRW = countsNorm./binSpans;

%% ====== figure 
close all

RTD_DNS = RTDs{iCase}(:,ixLocEnd+1);
RTD_DNS = RTD_DNS/trapz(centers,RTD_DNS);

loglog(centers, RTD_DNS,'r-','linewidth',2); hold on     
loglog(centers, RTD_CTRW,'bo','markersize',5); 

xlim([1e-1 1e+3])
ylim([0.9e-5 2.2e+0])

legend('DNS','CTRW')
legend box off
xlabel('time [PVI]')
ylabel('PDF')

ax = gca;
ax.FontSize = 14;
ax.XTick = 10.^[-1:1:3];
ax.YTick = 10.^[-5:1:0];

set(gcf,'color','w')

print(sprintf('case%d.svg',iCase),'-dsvg')

end
